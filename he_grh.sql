-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : jeu. 06 avr. 2023 à 08:06
-- Version du serveur :  8.0.21
-- Version de PHP : 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `he_grh`
--

-- --------------------------------------------------------

--
-- Structure de la table `doctrine_migration_versions`
--

DROP TABLE IF EXISTS `doctrine_migration_versions`;
CREATE TABLE IF NOT EXISTS `doctrine_migration_versions` (
  `version` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `doctrine_migration_versions`
--

INSERT INTO `doctrine_migration_versions` (`version`, `executed_at`, `execution_time`) VALUES
('DoctrineMigrations\\Version20230403213137', '2023-04-03 21:32:09', 31747),
('DoctrineMigrations\\Version20230405060755', '2023-04-05 06:08:12', 10708),
('DoctrineMigrations\\Version20230405062650', '2023-04-05 06:26:58', 1451),
('DoctrineMigrations\\Version20230405071744', '2023-04-05 07:19:09', 23132),
('DoctrineMigrations\\Version20230405075620', '2023-04-05 07:56:29', 5839),
('DoctrineMigrations\\Version20230405093009', '2023-04-05 09:30:23', 12916),
('DoctrineMigrations\\Version20230405111321', '2023-04-05 11:13:28', 16809);

-- --------------------------------------------------------

--
-- Structure de la table `he_degrees`
--

DROP TABLE IF EXISTS `he_degrees`;
CREATE TABLE IF NOT EXISTS `he_degrees` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `he_faculties`
--

DROP TABLE IF EXISTS `he_faculties`;
CREATE TABLE IF NOT EXISTS `he_faculties` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `acronym` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `he_faculties`
--

INSERT INTO `he_faculties` (`id`, `name`, `acronym`, `description`) VALUES
(1, 'La fonction RH dans l’entreprise', '', 'La fonction RH dans l’entreprise'),
(2, 'Gérer et développer les compétences', '', 'Gérer et développer les compétences'),
(3, 'Initiation à la gestion industrielle', '', 'Initiation à la gestion industrielle'),
(4, 'Qualité et prévention', '', 'Qualité et prévention'),
(5, 'Communication orale et écrite', '', 'Communication orale et écrite'),
(6, 'Savoir communiquer et travailler dans une équipe multidisciplinaire', '', 'Savoir communiquer et travailler dans une équipe multidisciplinaire'),
(7, 'S’affirmer dans ses relations professionnelles', '', 'S’affirmer dans ses relations professionnelles'),
(8, 'Animation d’un groupe de projet/groupe de travail', '', 'Animation d’un groupe de projet/groupe de travail'),
(9, 'Droit du travail et relation sociale', '', 'Droit du travail et relation sociale'),
(10, 'Paie et politique de rémunération', '', 'Paie et politique de rémunération'),
(11, 'Prévention des risques professionnels', '', 'Prévention des risques professionnels'),
(12, 'Les outils de la résolution de problème et amélioration continue', '', 'Les outils de la résolution de problème et amélioration continue'),
(13, 'Gestion du personnel', '', 'Gestion du personnel'),
(14, 'Gérer une situation conflictuelle', '', 'Gérer une situation conflictuelle'),
(15, 'Mieux gérer son temps et ses priorités', '', 'Mieux gérer son temps et ses priorités');

-- --------------------------------------------------------

--
-- Structure de la table `he_levels`
--

DROP TABLE IF EXISTS `he_levels`;
CREATE TABLE IF NOT EXISTS `he_levels` (
  `id` int NOT NULL AUTO_INCREMENT,
  `degree_id` int NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `IDX_62CCC529B35C5756` (`degree_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `he_pages`
--

DROP TABLE IF EXISTS `he_pages`;
CREATE TABLE IF NOT EXISTS `he_pages` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `view` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `link_text` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `menu_order` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `he_pages`
--

INSERT INTO `he_pages` (`id`, `title`, `slug`, `description`, `view`, `is_active`, `is_deleted`, `link_text`, `menu_order`) VALUES
(1, 'HE-GRH', 'welcome', 'Page d\'acceuil', 'page/index.html', 1, 0, 'Acceuil', 1);

-- --------------------------------------------------------

--
-- Structure de la table `he_page_he_page`
--

DROP TABLE IF EXISTS `he_page_he_page`;
CREATE TABLE IF NOT EXISTS `he_page_he_page` (
  `he_page_source` int NOT NULL,
  `he_page_target` int NOT NULL,
  PRIMARY KEY (`he_page_source`,`he_page_target`),
  KEY `IDX_F54D64A68D77C69` (`he_page_source`),
  KEY `IDX_F54D64A71322CE6` (`he_page_target`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `he_payments`
--

DROP TABLE IF EXISTS `he_payments`;
CREATE TABLE IF NOT EXISTS `he_payments` (
  `id` int NOT NULL AUTO_INCREMENT,
  `student_id` int NOT NULL,
  `reason` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `decision` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `observation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `validated_at` datetime DEFAULT NULL,
  `done_at` date NOT NULL,
  `reference_mobile_money` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_1B45C359CB944F1A` (`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `he_roles`
--

DROP TABLE IF EXISTS `he_roles`;
CREATE TABLE IF NOT EXISTS `he_roles` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `he_semesters`
--

DROP TABLE IF EXISTS `he_semesters`;
CREATE TABLE IF NOT EXISTS `he_semesters` (
  `id` int NOT NULL AUTO_INCREMENT,
  `level_id` int NOT NULL,
  `number` int NOT NULL,
  `year` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_1F5C46C85FB14BA7` (`level_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `he_student_informations`
--

DROP TABLE IF EXISTS `he_student_informations`;
CREATE TABLE IF NOT EXISTS `he_student_informations` (
  `id` int NOT NULL AUTO_INCREMENT,
  `level_id` int NOT NULL,
  `faculty_id` int NOT NULL,
  `matricule_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `employer_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `job` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fm_names` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fm_jobs` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fm_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fm_phone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fm_adress` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `t_names` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `t_jobs` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `t_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `t_phone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `t_adress` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `g_names` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `g_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `g_phone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `formation_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cin_photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `curriculum_vitae` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note_or_copy_of_the_certified_diploma` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `residence_certificate` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birth_certificate` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `motivation_letter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sexe` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `native_country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `semester_id` int DEFAULT NULL,
  `vague_id` int DEFAULT NULL,
  `registration_fee` int DEFAULT NULL,
  `monthly_fees` int DEFAULT NULL,
  `examination_fees` int DEFAULT NULL,
  `defense_fee` int DEFAULT NULL,
  `certificate_fee` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_76E3AA8E5FB14BA7` (`level_id`),
  KEY `IDX_76E3AA8E680CAB68` (`faculty_id`),
  KEY `IDX_76E3AA8E4A798B6F` (`semester_id`),
  KEY `IDX_76E3AA8E93E74B61` (`vague_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `he_users`
--

DROP TABLE IF EXISTS `he_users`;
CREATE TABLE IF NOT EXISTS `he_users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `student_information_id` int DEFAULT NULL,
  `email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birth_date` datetime DEFAULT NULL,
  `birth_place` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `confirm_at` datetime DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `is_verified` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_90DBEFF3E7927C74` (`email`),
  KEY `IDX_90DBEFF3391213BA` (`student_information_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `he_user_he_role`
--

DROP TABLE IF EXISTS `he_user_he_role`;
CREATE TABLE IF NOT EXISTS `he_user_he_role` (
  `he_user_id` int NOT NULL,
  `he_role_id` int NOT NULL,
  PRIMARY KEY (`he_user_id`,`he_role_id`),
  KEY `IDX_EE5A1F9F814A0598` (`he_user_id`),
  KEY `IDX_EE5A1F9FF027F4A1` (`he_role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `he_vagues`
--

DROP TABLE IF EXISTS `he_vagues`;
CREATE TABLE IF NOT EXISTS `he_vagues` (
  `id` int NOT NULL AUTO_INCREMENT,
  `v_number` int NOT NULL,
  `v_description` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `reset_password_request`
--

DROP TABLE IF EXISTS `reset_password_request`;
CREATE TABLE IF NOT EXISTS `reset_password_request` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `selector` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hashed_token` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `requested_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `expires_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  PRIMARY KEY (`id`),
  KEY `IDX_7CE748AA76ED395` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `he_levels`
--
ALTER TABLE `he_levels`
  ADD CONSTRAINT `FK_62CCC529B35C5756` FOREIGN KEY (`degree_id`) REFERENCES `he_degrees` (`id`);

--
-- Contraintes pour la table `he_page_he_page`
--
ALTER TABLE `he_page_he_page`
  ADD CONSTRAINT `FK_F54D64A68D77C69` FOREIGN KEY (`he_page_source`) REFERENCES `he_pages` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_F54D64A71322CE6` FOREIGN KEY (`he_page_target`) REFERENCES `he_pages` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `he_payments`
--
ALTER TABLE `he_payments`
  ADD CONSTRAINT `FK_1B45C359CB944F1A` FOREIGN KEY (`student_id`) REFERENCES `he_student_informations` (`id`);

--
-- Contraintes pour la table `he_semesters`
--
ALTER TABLE `he_semesters`
  ADD CONSTRAINT `FK_1F5C46C85FB14BA7` FOREIGN KEY (`level_id`) REFERENCES `he_levels` (`id`);

--
-- Contraintes pour la table `he_student_informations`
--
ALTER TABLE `he_student_informations`
  ADD CONSTRAINT `FK_76E3AA8E4A798B6F` FOREIGN KEY (`semester_id`) REFERENCES `he_semesters` (`id`),
  ADD CONSTRAINT `FK_76E3AA8E5FB14BA7` FOREIGN KEY (`level_id`) REFERENCES `he_levels` (`id`),
  ADD CONSTRAINT `FK_76E3AA8E680CAB68` FOREIGN KEY (`faculty_id`) REFERENCES `he_faculties` (`id`),
  ADD CONSTRAINT `FK_76E3AA8E93E74B61` FOREIGN KEY (`vague_id`) REFERENCES `he_vagues` (`id`);

--
-- Contraintes pour la table `he_users`
--
ALTER TABLE `he_users`
  ADD CONSTRAINT `FK_90DBEFF3391213BA` FOREIGN KEY (`student_information_id`) REFERENCES `he_student_informations` (`id`);

--
-- Contraintes pour la table `he_user_he_role`
--
ALTER TABLE `he_user_he_role`
  ADD CONSTRAINT `FK_EE5A1F9F814A0598` FOREIGN KEY (`he_user_id`) REFERENCES `he_users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_EE5A1F9FF027F4A1` FOREIGN KEY (`he_role_id`) REFERENCES `he_roles` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `reset_password_request`
--
ALTER TABLE `reset_password_request`
  ADD CONSTRAINT `FK_7CE748AA76ED395` FOREIGN KEY (`user_id`) REFERENCES `he_users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
