<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;


class SecurityController extends AbstractController
{
    /**
     * @Route("/login-bo", name="app_login_bo", priority=10)
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('app_home');
        }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login-bo.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * @Route("/check-user-connected/bo", name="app_login_success_check", priority=20)
     */
    public function checkUserConnectedBo(): Response
    {
        if (!$this->getUser()) {
            return $this->redirectToRoute('app_login_bo');
        } else {
            $user = $this->getUser();
            if (in_array("ROLE_ADMIN", $user->getRoles(), true)) {
                return $this->redirectToRoute('app_admin_home');
            } else if (in_array("ROLE_SCHOOL_MANAGER", $user->getRoles(), true)) {
                return $this->redirectToRoute('app_school_manager_home');
            } else if (in_array("ROLE_TEACHER", $user->getRoles(), true)) {
                return $this->redirectToRoute('app_teacher_home');
            }
        }
        return $this->redirectToRoute('app_login_bo');
    }

    /**
     * @Route("/logout", name="app_logout",priority=9)
     */
    public function logout(): void
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }
}
