<?php

namespace App\Controller\SchoolManager;

use DateTime;
use App\Entity\HePayment;
use Symfony\Component\Mime\Email;
use App\Repository\HeUserRepository;
use App\Repository\HePaymentRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/school-manager", priority=10)
 */
class SchoolManagerController extends AbstractController
{
    /**
     * @Route("/", name="app_school_manager_home")
     */
    public function index(): Response
    {
        return $this->render('school_manager/dashboard.html.twig');
    }

    /**
     * @Route("/payment/request/{type}", name="app_school_manager_payment_request")
     */
    public function paymentRequest(?string $type, Request $request, HePaymentRepository $hePaymentRepository): Response
    {
        $requests = $hePaymentRepository->getRequestUnReadByType($type);
        return $this->render('school_manager/payment/request.html.twig', [
            'requests' => $requests,
            'type' => $type
        ]);
    }


    /**
     * @Route("/payment/validate/{id}", name="app_payment_validate")
     */
    public function paymentValidate(
        HePayment $hePayment,
        Request $request,
        MailerInterface $mailer,
        EntityManagerInterface $entityManagerInterface,
        HeUserRepository $heUserRepository
    ): Response {
        $hePayment
            ->setState('read')
            ->setDecision('taken')
            ->setObservation($request->request->get('observation_' . $hePayment->getId()))
            ->setValidatedAt(new \DateTime());
        $studentInformation = $hePayment->getStudent();
        switch ($hePayment->getReason()) {
            case 'registration-fee':
                $studentInformation->setRegistrationFee(1);
                break;
            case 'monthly-fees':
                $studentInformation->setMonthlyFees($studentInformation->getMonthlyFees() + ($hePayment->getAmount() / 200000));
                break;
        }
        $student = $heUserRepository->getUserByStudentInformation($studentInformation);
        $email = (new TemplatedEmail())
            ->from('aeli@gmail.com')
            ->to($student->getEmail())
            ->subject('aeli paiement : ' . $this->getReasonPayment($hePayment->getReason()) . ' par MVOLA')
            ->htmlTemplate("school_manager/payment/email-template.html.twig")
            ->context([
                'reason' =>  $hePayment->getReason(),
                'id' =>  $hePayment->getId(),
                'student' => $student
            ]);
        $mailer->send($email);
        $entityManagerInterface->flush();
        $this->addFlash('success', "Une demande a été validé avec succèss, IDENTIFICATION : " . $hePayment->getId());
        return $this->redirectToRoute('app_school_manager_payment_request', ['type' => $hePayment->getType()]);
    }


    public function getReasonPayment($reason)
    {
        return [
            'registration-fee' => 'Frais d\'inscription',
            'monthly-fees' => 'Écolage',
        ][$reason];
    }
}
