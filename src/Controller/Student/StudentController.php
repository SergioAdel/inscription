<?php

namespace App\Controller\Student;

use App\Repository\HeUserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/student", priority=10)
 */
class StudentController extends AbstractController
{
    /**
     * @Route("/", name="app_student_home")
     */
    public function index(Request $request): Response
    {
        // on vérifie si les registrationFee n'est pas payé
        $studentConnected = $this->getUser();
        $studentInformation = $studentConnected->getStudentInformation();
        $registrationFee = $studentInformation->getRegistrationFee();
        if ($registrationFee) {
            $monthlyFees = $studentInformation->getMonthlyFees();

            if ($monthlyFees == 0) {
                return $this->redirectToRoute('app_payment_monthly_fee');
            }
        } else {
            return $this->redirectToRoute('app_student_validate_account');
        }
        return $this->render('student/dashboard.html.twig', [
            'disableAllMenu' => false,
            'activeMenu' => " none",
        ]);
    }

    /**
     * @Route("/validate-account", name="app_student_validate_account")
     */
    public function validateAccount(Request $request): Response
    {
        $disableAllMenus = true;
        return $this->render('student/validate-account.html.twig', [
            'disableAllMenu' => $disableAllMenus,
            'activeMenu' => " none",
        ]);
    }
}
