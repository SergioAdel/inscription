<?php

namespace App\Controller\Admin;

use App\Entity\HePage;
use App\Form\HePageType;
use App\Repository\HePageRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Twig\Environment;

/**
 * @Route("/admin", priority=10)
 */
class HePageController extends AbstractController
{
    /**
     * @Route("/page", name="app_admin_page")
     */
    public function index(HePageRepository $hePageRepository): Response
    {
        return $this->render('admin/pages/index.html.twig', [
            'pages' => $hePageRepository->findNotDeletedPage()
        ]);
    }
    /**
     * @Route("/page/on", name="app_admin_page_on")
     */
    public function on(HePageRepository $hePageRepository): Response
    {
        return $this->render('admin/pages/on.html.twig', [
            'pages' => $hePageRepository->findNotDeletedPageOn()
        ]);
    }
    /**
     * @Route("/page/off", name="app_admin_page_off")
     */
    public function off(HePageRepository $hePageRepository): Response
    {
        return $this->render('admin/pages/off.html.twig', [
            'pages' => $hePageRepository->findNotDeletedPageOff()
        ]);
    }

    /**
     * @Route("/page/render-view/{view}", name="app_admin_page_render_iframe", requirements={"view"=".+"})
     */
    public function render_iframe($view): Response
    {
        return $this->render($view . '.twig');
    }

    /**
     * @Route("/page/{id}/view", name="app_admin_page_view")
     */
    public function viewPage(HePage $hePage, Environment $twig): Response
    {
        $noView = false;
        if (!$twig->getLoader()->exists($hePage->getView() . '.twig')) $noView = true;
        return $this->render('admin/pages/view.html.twig', compact('hePage', 'noView'));
    }

    /**
     * @Route("/page/add", name="app_admin_page_add")
     */
    public function add(Request $request, EntityManagerInterface $em): Response
    {
        $hePage = new HePage();
        $hePage->setIsDeleted(false);
        $form = $this->createForm(HePageType::class, $hePage);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($hePage);
            $em->flush();
            $this->addFlash('success', 'Ajout d\'une nouvelle page ' . $hePage->getTitle() . ' avec succès');
            return $this->redirectToRoute('app_admin_page');
        }
        return $this->renderForm('admin/pages/add.html.twig', compact('form'));
    }

    /**
     * @Route("/page/{id}/edit", name="app_admin_page_edit")
     */
    public function edit(HePage $hePage, Request $request, EntityManagerInterface $em): Response
    {
        $form = $this->createForm(HePageType::class, $hePage);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();
            $this->addFlash('success', 'Modification de la page ' . $hePage->getTitle() . ' avec succès');
            return $this->redirectToRoute('app_admin_page');
        }
        return $this->renderForm('admin/pages/edit.html.twig', compact('hePage', 'form'));
    }

    /**
     * @Route("/page/{id}/toggle/active", name="app_admin_page_toggle")
     */
    public function toogle(HePage $hePage, EntityManagerInterface $em): Response
    {
        if ($hePage->isIsActive()) {
            $this->addFlash('danger', 'Désactivation de la page "' . $hePage->getTitle() . '" avec succèss');
        } else {
            $this->addFlash('success', 'L\'activation de la page "' . $hePage->getTitle() . '" avec succèss');
        }
        $hePage->setIsActive(!$hePage->isIsActive());
        $em->flush();
        return $this->redirectToRoute('app_admin_page');
    }

    /**
     * @Route("/page/{id}/delete", name="app_admin_page_delete")
     */
    public function delete(HePage $hePage, EntityManagerInterface $em): Response
    {
        $hePage->setIsDeleted(true);
        $em->flush();
        $this->addFlash('success', 'Suppression de la page "' . $hePage->getTitle() . '" avec succèss');
        return $this->redirectToRoute('app_admin_page');
    }
}
