<?php

namespace App\Controller\Admin;

use App\Repository\HeUserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin", priority=10)
 */
class AdminController extends AbstractController
{
    /**
     * @Route("/", name="app_admin_home")
     */
    public function index(Request $request, HeUserRepository $heUserRepository): Response
    {
        $users = $heUserRepository->getUserByRoleType($request->query->get('type', 'ROLE_STUDENT'));
        return $this->render('admin/dashboard.html.twig', compact('users'));
    }
}
