<?php

namespace App\Controller\Admin;

use App\Entity\HeRole;
use App\Entity\HeStudentInformation;
use App\Entity\HeUser;
use App\Form\UserFormType;
use App\Security\EmailVerifier;
use Symfony\Component\Mime\Address;
use App\Repository\HeRoleRepository;
use App\Repository\HeUserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

/**
 * @Route("/admin", priority=10)
 */
class HeUserController extends AbstractController
{
    private $emailVerifier;

    public function __construct(EmailVerifier $emailVerifier)
    {
        $this->emailVerifier = $emailVerifier;
    }

    private function getLastRoute(Request $request)
    {
        $referer = $request->headers->get('referer');
        $baseUrl = $request->getBaseUrl();
        return substr($referer, strpos($referer, $baseUrl) + strlen($baseUrl));
    }
    /**
     * @Route("/user", name="app_admin_user")
     */
    public function index(HeUserRepository $heUserRepository, Request $request): Response
    {
        $users = $heUserRepository->getUserByRoleType($request->query->get('type', 'ROLE_STUDENT'));
        $usersNonConfirm = $heUserRepository->getUserByRoleTypeNonConfirm($request->query->get('type', 'ROLE_STUDENT'));
        $usersNonVerified = $heUserRepository->getUserByRoleTypeNonVerified($request->query->get('type', 'ROLE_STUDENT'));
        return $this->render('admin/users/index.html.twig', compact('users', 'usersNonConfirm', 'usersNonVerified'));
    }

    /**
     * @Route("/user/non-confirm", name="app_admin_user_non_confirm")
     */
    public function nonConfirmUser(HeUserRepository $heUserRepository, Request $request): Response
    {
        $users = $heUserRepository->getUserByRoleTypeNonConfirm($request->query->get('type', 'ROLE_STUDENT'));
        return $this->render('admin/users/non-confirm.html.twig', compact('users'));
    }

    /**
     * @Route("/user/non-verified", name="app_admin_user_non_verified")
     */
    public function nonVerifiedUser(HeUserRepository $heUserRepository, Request $request): Response
    {
        $users = $heUserRepository->getUserByRoleTypeNonVerified($request->query->get('type', 'ROLE_STUDENT'));
        return $this->render('admin/users/non-verified.html.twig', compact('users'));
    }

    /**
     * @Route("/user/{id<\d+>}/confirm", name="app_admin_user_confirm")
     */
    public function confirm(HeUser $heUser, EntityManagerInterface $entityManagerInterface, Request $request): Response
    {
        $heUser->setConfirmAt(new \Datetime('now'));
        $entityManagerInterface->flush();
        $this->addFlash('success', 'Confirmation d\'utilisateur : "' . $heUser->getFullName() . '" avec succès');
        return $this->redirect($this->getLastRoute($request));
    }

    /**
     * @Route("/user/{id<\d+>}/verify", name="app_admin_user_verify")
     */
    public function verify(HeUser $heUser, EntityManagerInterface $entityManagerInterface, Request $request): Response
    {
        $heUser->setIsVerified(true);
        $entityManagerInterface->flush();
        $this->addFlash('success', 'Vérification d\'utilisateur : "' . $heUser->getFullName() . '" avec succès');
        return $this->redirect($this->getLastRoute($request));
    }

    /**
     * @Route("/user/add", name="app_admin_user_add")
     */
    public function add(Request $request, UserPasswordHasherInterface $userPasswordHasher, EntityManagerInterface $entityManager, HeRoleRepository $heRoleRepository): Response
    {
        $role = $request->query->get('type', 'ROLE_STUDENT');
        $user = new HeUser();
        $user->setIsDeleted(false);
        $user->addHeRole($heRoleRepository->findOneByName($role));
        $user->setBirthDate(new \DateTime());
        $form = $this->createForm(UserFormType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $user->setPassword(
                $userPasswordHasher->hashPassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );

            $entityManager->persist($user);
            $entityManager->flush();

            $this->emailVerifier->sendEmailConfirmation(
                'app_verify_email',
                $user,
                (new TemplatedEmail())
                    ->from(new Address('aeli-africa@gmail.com', 'AELI AFRICA'))
                    ->to($user->getEmail())
                    ->subject('Please Confirm your Email')
                    ->htmlTemplate('registration/confirmation_email.html.twig')
            );
            $this->addFlash('success', 'Création d\'une nouvelle compte avec succèss, une E-mail a été envoyé vers : ' . $user->getEmail() . ' pour la vérification de cette compte.');
            return $this->redirect($this->generateUrl('app_admin_user') . '?type=' . $request->query->get('type', 'ROLE_STUDENT'));
        }

        return $this->render('admin/users/add.html.twig', [
            'form' => $form->createView(),
        ]);

        return $this->render('admin/users/add.html.twig');
    }





    // REMOVE STUDENT

    /**
     * @Route("/user/student/{id<\d+>}/remove", name="app_admin_user_student_remove")
     */
    public function removeStudent(HeStudentInformation $student, EntityManagerInterface $entityManagerInterface): Response
    {
        $entityManagerInterface->remove($student);
        $entityManagerInterface->flush();
        return $this->render('admin/users/index.html.twig', [
            'users' => []
        ]);
    }





    // MENU INSCRIPTION

    /**
     * @Route("/user/student/registration-request", name="app_admin_user_student_registration_request")
     */
    public function registrationRequest(HeUserRepository $heUserRepository, Request $request): Response
    {
        $users = $heUserRepository->getUserByRoleType($request->query->get('type', 'ROLE_STUDENT'));
        return $this->render('admin/users/student/registration-request.html.twig', [
            'users' => $users
        ]);
    }
}
