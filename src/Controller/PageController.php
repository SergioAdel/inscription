<?php

namespace App\Controller;

use App\Entity\HePage;
use Symfony\Component\Mime\Email;
use App\Repository\HePageRepository;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class PageController extends AbstractController
{
    /**
     * @Route("/", name="app_home", priority=0)
     */
    public function index(): Response
    {
        return $this->redirectToRoute('app_page', ["slug" => "welcome"]);
    }

    /**
     * @Route("/{slug}", name="app_page", priority=2)
     */
    public function show(HePage $hePage): Response
    {
        return $this->render($hePage->getView() . '.twig', compact('hePage'));
    }

    /**
     * @Route("/a-propos", name="a_propos", priority=2)
     */

    public function apropos(): Response
    {
        return $this->render('page/apropos.html.twig');
    }
}
