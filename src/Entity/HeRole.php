<?php

namespace App\Entity;

use App\Repository\HeRoleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=HeRoleRepository::class)
 * @ORM\Table(name="he_roles")
 */
class HeRole
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @ORM\ManyToMany(targetEntity=HeUser::class, mappedBy="heRoles")
     */
    private $heUsers;



    public function __construct()
    {
        $this->heUsers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection<int, HeUser>
     */
    public function getHeUsers(): Collection
    {
        return $this->heUsers;
    }

    public function addHeUser(HeUser $heUser): self
    {
        if (!$this->heUsers->contains($heUser)) {
            $this->heUsers[] = $heUser;
            $heUser->addHeRole($this);
        }

        return $this;
    }

    public function removeHeUser(HeUser $heUser): self
    {
        if ($this->heUsers->removeElement($heUser)) {
            $heUser->removeHeRole($this);
        }

        return $this;
    }
}
