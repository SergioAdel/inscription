<?php

namespace App\Entity;

use App\Repository\HeStudentInformationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=HeStudentInformationRepository::class)
 * @ORM\Table(name="he_student_informations")
 */
class HeStudentInformation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $matriculeNumber;

    /**
     * @ORM\ManyToOne(targetEntity=HeLevel::class, inversedBy="studentInformations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $level;

    /**
     * @ORM\ManyToOne(targetEntity=HeFaculty::class, inversedBy="studentInformations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $faculty;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $employerName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $job;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $fmNames;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $fmJobs;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $fmEmail;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $fmPhoneNumber;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $fmAdress;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $tNames;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $tJobs;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $tEmail;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $tPhoneNumber;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $tAdress;

    /**
     * @ORM\OneToMany(targetEntity=HeUser::class, mappedBy="studentInformation")
     */
    private $users;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $gNames;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $gEmail;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $gPhoneNumber;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $formationType;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $cinPhoto;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $curriculumVitae;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $noteOrCopyOfTheCertifiedDiploma;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $residenceCertificate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $birthCertificate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $motivationLetter;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $sexe;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nativeCountry;

    /**
     * @ORM\ManyToOne(targetEntity=HeSemester::class, inversedBy="studentInformation")
     */
    private $semester;

    /**
     * @ORM\ManyToOne(targetEntity=HeVague::class, inversedBy="studentInformation")
     */
    private $vague;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $registrationFee;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $monthlyFees;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $examinationFees;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $defenseFee;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $certificateFee;

    /**
     * @ORM\OneToMany(targetEntity=HePayment::class, mappedBy="student")
     */
    private $payments;

    



    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->payments = new ArrayCollection();
       
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMatriculeNumber(): ?string
    {
        return $this->matriculeNumber;
    }

    public function setMatriculeNumber(?string $matriculeNumber): self
    {
        $this->matriculeNumber = $matriculeNumber;

        return $this;
    }

    public function getLevel(): ?HeLevel
    {
        return $this->level;
    }

    public function setLevel(?HeLevel $level): self
    {
        $this->level = $level;

        return $this;
    }

    public function getFaculty(): ?HeFaculty
    {
        return $this->faculty;
    }

    public function setFaculty(?HeFaculty $faculty): self
    {
        $this->faculty = $faculty;

        return $this;
    }

    public function getEmployerName(): ?string
    {
        return $this->employerName;
    }

    public function setEmployerName(?string $employerName): self
    {
        $this->employerName = $employerName;

        return $this;
    }

    public function getJob(): ?string
    {
        return $this->job;
    }

    public function setJob(?string $job): self
    {
        $this->job = $job;

        return $this;
    }

    /**
     * @return Collection<int, HeUser>
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(HeUser $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->setStudentInformation($this);
        }

        return $this;
    }

    public function removeUser(HeUser $user): self
    {
        if ($this->users->removeElement($user)) {
            // set the owning side to null (unless already changed)
            if ($user->getStudentInformation() === $this) {
                $user->setStudentInformation(null);
            }
        }

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getFmNames(): ?string
    {
        return $this->fmNames;
    }

    public function setFmNames(?string $fmNames): self
    {
        $this->fmNames = $fmNames;

        return $this;
    }

    public function getFmJobs(): ?string
    {
        return $this->fmJobs;
    }

    public function setFmJobs(?string $fmJobs): self
    {
        $this->fmJobs = $fmJobs;

        return $this;
    }

    public function getFmEmail(): ?string
    {
        return $this->fmEmail;
    }

    public function setFmEmail(?string $fmEmail): self
    {
        $this->fmEmail = $fmEmail;

        return $this;
    }

    public function getFmPhoneNumber(): ?string
    {
        return $this->fmPhoneNumber;
    }

    public function setFmPhoneNumber(?string $fmPhoneNumber): self
    {
        $this->fmPhoneNumber = $fmPhoneNumber;

        return $this;
    }

    public function getFmAdress(): ?string
    {
        return $this->fmAdress;
    }

    public function setFmAdress(?string $fmAdress): self
    {
        $this->fmAdress = $fmAdress;

        return $this;
    }

    public function getTNames(): ?string
    {
        return $this->tNames;
    }

    public function setTNames(?string $tNames): self
    {
        $this->tNames = $tNames;

        return $this;
    }

    public function getTJobs(): ?string
    {
        return $this->tJobs;
    }

    public function setTJobs(?string $tJobs): self
    {
        $this->tJobs = $tJobs;

        return $this;
    }

    public function getTEmail(): ?string
    {
        return $this->tEmail;
    }

    public function setTEmail(?string $tEmail): self
    {
        $this->tEmail = $tEmail;

        return $this;
    }

    public function getTPhoneNumber(): ?string
    {
        return $this->tPhoneNumber;
    }

    public function setTPhoneNumber(?string $tPhoneNumber): self
    {
        $this->tPhoneNumber = $tPhoneNumber;

        return $this;
    }

    public function getTAdress(): ?string
    {
        return $this->tAdress;
    }

    public function setTAdress(?string $tAdress): self
    {
        $this->tAdress = $tAdress;

        return $this;
    }

    public function getGNames(): ?string
    {
        return $this->gNames;
    }

    public function setGNames(?string $gNames): self
    {
        $this->gNames = $gNames;

        return $this;
    }

    public function getGEmail(): ?string
    {
        return $this->gEmail;
    }

    public function setGEmail(?string $gEmail): self
    {
        $this->gEmail = $gEmail;

        return $this;
    }

    public function getGPhoneNumber(): ?string
    {
        return $this->gPhoneNumber;
    }

    public function setGPhoneNumber(?string $gPhoneNumber): self
    {
        $this->gPhoneNumber = $gPhoneNumber;

        return $this;
    }

    public function getFormationType(): ?string
    {
        return $this->formationType;
    }

    public function setFormationType(string $formationType): self
    {
        $this->formationType = $formationType;

        return $this;
    }

    public function getCinPhoto(): ?string
    {
        return $this->cinPhoto;
    }

    public function setCinPhoto(?string $cinPhoto): self
    {
        $this->cinPhoto = $cinPhoto;

        return $this;
    }

    public function getCurriculumVitae(): ?string
    {
        return $this->curriculumVitae;
    }

    public function setCurriculumVitae(?string $curriculumVitae): self
    {
        $this->curriculumVitae = $curriculumVitae;

        return $this;
    }

    public function getNoteOrCopyOfTheCertifiedDiploma(): ?string
    {
        return $this->noteOrCopyOfTheCertifiedDiploma;
    }

    public function setNoteOrCopyOfTheCertifiedDiploma(?string $noteOrCopyOfTheCertifiedDiploma): self
    {
        $this->noteOrCopyOfTheCertifiedDiploma = $noteOrCopyOfTheCertifiedDiploma;

        return $this;
    }

    public function getResidenceCertificate(): ?string
    {
        return $this->residenceCertificate;
    }

    public function setResidenceCertificate(?string $residenceCertificate): self
    {
        $this->residenceCertificate = $residenceCertificate;

        return $this;
    }

    public function getBirthCertificate(): ?string
    {
        return $this->birthCertificate;
    }

    public function setBirthCertificate(?string $birthCertificate): self
    {
        $this->birthCertificate = $birthCertificate;

        return $this;
    }

    public function getMotivationLetter(): ?string
    {
        return $this->motivationLetter;
    }

    public function setMotivationLetter(?string $motivationLetter): self
    {
        $this->motivationLetter = $motivationLetter;

        return $this;
    }

    public function getSexe(): ?string
    {
        return $this->sexe;
    }

    public function setSexe(?string $sexe): self
    {
        $this->sexe = $sexe;

        return $this;
    }

    public function getNativeCountry(): ?string
    {
        return $this->nativeCountry;
    }

    public function setNativeCountry(?string $nativeCountry): self
    {
        $this->nativeCountry = $nativeCountry;

        return $this;
    }

    public function getSemester(): ?HeSemester
    {
        return $this->semester;
    }

    public function setSemester(?HeSemester $semester): self
    {
        $this->semester = $semester;

        return $this;
    }

    public function getVague(): ?HeVague
    {
        return $this->vague;
    }

    public function setVague(?HeVague $vague): self
    {
        $this->vague = $vague;

        return $this;
    }

    public function getRegistrationFee(): ?int
    {
        return $this->registrationFee;
    }

    public function setRegistrationFee(?int $registrationFee): self
    {
        $this->registrationFee = $registrationFee;

        return $this;
    }

    public function getMonthlyFees(): ?int
    {
        return $this->monthlyFees;
    }

    public function setMonthlyFees(?int $monthlyFees): self
    {
        $this->monthlyFees = $monthlyFees;

        return $this;
    }

    public function getExaminationFees(): ?int
    {
        return $this->examinationFees;
    }

    public function setExaminationFees(?int $examinationFees): self
    {
        $this->examinationFees = $examinationFees;

        return $this;
    }

    public function getDefenseFee(): ?int
    {
        return $this->defenseFee;
    }

    public function setDefenseFee(?int $defenseFee): self
    {
        $this->defenseFee = $defenseFee;

        return $this;
    }

    public function getCertificateFee(): ?int
    {
        return $this->certificateFee;
    }

    public function setCertificateFee(?int $certificateFee): self
    {
        $this->certificateFee = $certificateFee;

        return $this;
    }

    /**
     * @return Collection<int, HePayment>
     */
    public function getPayments(): Collection
    {
        return $this->payments;
    }

    public function addPayment(HePayment $payment): self
    {
        if (!$this->payments->contains($payment)) {
            $this->payments[] = $payment;
            $payment->setStudent($this);
        }

        return $this;
    }

    public function removePayment(HePayment $payment): self
    {
        if ($this->payments->removeElement($payment)) {
            // set the owning side to null (unless already changed)
            if ($payment->getStudent() === $this) {
                $payment->setStudent(null);
            }
        }

        return $this;
    }

}
