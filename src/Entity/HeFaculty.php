<?php

namespace App\Entity;

use App\Repository\HeFacultyRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=HeFacultyRepository::class)
 * @ORM\Table(name="he_faculties")
 */
class HeFaculty
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $acronym;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity=HeStudentInformation::class, mappedBy="faculty")
     */
    private $studentInformations;

    /**
     * @ORM\OneToMany(targetEntity=Hefaculty::class, mappedBy="parent")
     */
    private $hefaculties;

   

    public function __construct()
    {
        $this->studentInformations = new ArrayCollection();
        $this->hefaculties = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAcronym(): ?string
    {
        return $this->acronym;
    }

    public function setAcronym(string $acronym): self
    {
        $this->acronym = $acronym;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection<int, HeStudentInformation>
     */
    public function getStudentInformations(): Collection
    {
        return $this->studentInformations;
    }

    public function addStudentInformations(HeStudentInformation $student): self
    {
        if (!$this->studentInformations->contains($student)) {
            $this->studentInformations[] = $student;
            $student->setFaculty($this);
        }

        return $this;
    }

    public function removeStudentInformations(HeStudentInformation $student): self
    {
        if ($this->studentInformations->removeElement($student)) {
            // set the owning side to null (unless already changed)
            if ($student->getFaculty() === $this) {
                $student->setFaculty(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->getName();
    }

    /**
     * @return Collection<int, Hefaculty>
     */
    public function getHefaculties(): Collection
    {
        return $this->hefaculties;
    }

    public function addHefaculty(Hefaculty $hefaculty): self
    {
        if (!$this->hefaculties->contains($hefaculty)) {
            $this->hefaculties[] = $hefaculty;
            $hefaculty->setParent($this);
        }

        return $this;
    }

    public function removeHefaculty(Hefaculty $hefaculty): self
    {
        if ($this->hefaculties->removeElement($hefaculty)) {
            // set the owning side to null (unless already changed)
            if ($hefaculty->getParent() === $this) {
                $hefaculty->setParent(null);
            }
        }

        return $this;
    }
}
