<?php

namespace App\Entity;

use App\Repository\HeLevelRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=HeLevelRepository::class)
 * @ORM\Table(name="he_levels")
 */
class HeLevel
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity=HeDegree::class, inversedBy="levels")
     * @ORM\JoinColumn(nullable=false)
     */
    private $degree;

    /**
     * @ORM\OneToMany(targetEntity=HeStudentInformation::class, mappedBy="level")
     */
    private $studentInformations;

    /**
     * @ORM\OneToMany(targetEntity=HeSemester::class, mappedBy="level")
     */
    private $semesters;

    public function __construct()
    {
        $this->studentInformations = new ArrayCollection();
        $this->semesters = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getDegree(): ?HeDegree
    {
        return $this->degree;
    }

    public function setDegree(?HeDegree $degree): self
    {
        $this->degree = $degree;

        return $this;
    }

    /**
     * @return Collection<int, HeStudentInformation>
     */
    public function getStudentInformations(): Collection
    {
        return $this->studentInformations;
    }

    public function addStudentInformation(HeStudentInformation $student): self
    {
        if (!$this->studentInformations->contains($student)) {
            $this->studentInformations[] = $student;
            $student->setLevel($this);
        }

        return $this;
    }

    public function removeStudentInformation(HeStudentInformation $student): self
    {
        if ($this->studentInformations->removeElement($student)) {
            // set the owning side to null (unless already changed)
            if ($student->getLevel() === $this) {
                $student->setLevel(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->getName();
    }

    /**
     * @return Collection<int, HeSemester>
     */
    public function getSemesters(): Collection
    {
        return $this->semesters;
    }

    public function addSemester(HeSemester $semester): self
    {
        if (!$this->semesters->contains($semester)) {
            $this->semesters[] = $semester;
            $semester->setLevel($this);
        }

        return $this;
    }

    public function removeSemester(HeSemester $semester): self
    {
        if ($this->semesters->removeElement($semester)) {
            // set the owning side to null (unless already changed)
            if ($semester->getLevel() === $this) {
                $semester->setLevel(null);
            }
        }

        return $this;
    }
}
