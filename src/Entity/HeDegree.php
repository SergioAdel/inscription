<?php

namespace App\Entity;

use App\Repository\HeDegreeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=HeDegreeRepository::class)
 * @ORM\Table(name="he_degrees")
 */
class HeDegree
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity=HeLevel::class, mappedBy="degree", orphanRemoval=true)
     */
    private $levels;


   

    public function __construct()
    {
        $this->levels = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection<int, HeLevel>
     */
    public function getLevels(): Collection
    {
        return $this->levels;
    }

    public function addLevel(HeLevel $level): self
    {
        if (!$this->levels->contains($level)) {
            $this->levels[] = $level;
            $level->setDegree($this);
        }

        return $this;
    }

    public function removeLevel(HeLevel $level): self
    {
        if ($this->levels->removeElement($level)) {
            // set the owning side to null (unless already changed)
            if ($level->getDegree() === $this) {
                $level->setDegree(null);
            }
        }

        return $this;
    }

   
}
