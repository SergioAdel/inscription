<?php

namespace App\Entity;

use App\Repository\HeVagueRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=HeVagueRepository::class)
 * @ORM\Table(name="he_vagues")
 */
class HeVague
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $vNumber;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $vDescription;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getVNumber(): ?int
    {
        return $this->vNumber;
    }

    public function setVNumber(int $vNumber): self
    {
        $this->vNumber = $vNumber;

        return $this;
    }

    public function getVDescription(): ?string
    {
        return $this->vDescription;
    }

    public function setVDescription(?string $vDescription): self
    {
        $this->vDescription = $vDescription;

        return $this;
    }
}
