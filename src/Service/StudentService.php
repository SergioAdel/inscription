<?php

namespace App\Service;

use App\Entity\HeStudentInformation;
use App\Repository\HeLevelRepository;
use App\Repository\HeFacultyRepository;
use App\Repository\HeStudentInformationRepository;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class StudentService
{

    private $heLevelRepository;
    private $heFacultyRepository;
    private $heStudentInformationRepository;
    private $slugger;

    private $cinPhoto;
    private $curriculumVitae;
    private $noteOrCopyOfTheCertifiedDiploma;
    private $residenceCertificate;
    private $birthCertificate;
    private $motivationLetter;

    public function __construct(
        HeLevelRepository $heLevelRepository,
        HeFacultyRepository $heFacultyRepository,
        HeStudentInformationRepository $heStudentInformationRepository,
        SluggerInterface $slugger,
        $cinPhoto,
        $curriculumVitae,
        $noteOrCopyOfTheCertifiedDiploma,
        $residenceCertificate,
        $birthCertificate,
        $motivationLetter
    ) {
        $this->heLevelRepository = $heLevelRepository;
        $this->heFacultyRepository = $heFacultyRepository;
        $this->heStudentInformationRepository = $heStudentInformationRepository;
        $this->slugger = $slugger;

        $this->cinPhoto = $cinPhoto;
        $this->curriculumVitae = $curriculumVitae;
        $this->noteOrCopyOfTheCertifiedDiploma = $noteOrCopyOfTheCertifiedDiploma;
        $this->residenceCertificate = $residenceCertificate;
        $this->birthCertificate = $birthCertificate;
        $this->motivationLetter = $motivationLetter;
    }

    public function generateMatriculeNumber(HeStudentInformation $heStudentInformation, $number): string
    {
        $formationType = $heStudentInformation->getFormationType();
        $matriculeNumber = $matriculeNumber = $heStudentInformation->getFaculty()->getAcronym() . '-V' . $ieStudentInformation->getVague()->getVNumber() . '/' . $number;
        if ($formationType == "online") {
            $matriculeNumber .= "/FLI";
        } else if ($formationType == "face-to-face") {
            $matriculeNumber .= "/FPR";
        }
        return $matriculeNumber;
    }

    public function getLastNumberOfMatriculeNumber($faculty): string
    {
        $studentInformations = $this->heStudentInformationRepository->getMatriculeNumberByFaculty($faculty);
        $studentInformationMatricules = [];
        foreach ($studentInformations as $studentInformation) {
            $studentInformationMatricules[] = $studentInformation->getMatriculeNumber();
        }

        $existingNumbers = [];
        foreach ($studentInformationMatricules as $matriculeNumber) {
            $existingNumbers[] = $this->decodeMatriculeNumber($matriculeNumber)['number'];
        }

        $existingNumbers = array_unique($existingNumbers);
        sort($existingNumbers);
        return $existingNumbers[count($existingNumbers) - 1];
    }

    public function decodeMatriculeNumber($matriculeNumber): array
    {
        // DAPT-V1/1/MG
        $a1 = explode('-', $matriculeNumber);
        $a2 = explode('/', $a1[1]);
        return [
            'acronymFaculty' => $a1[1],
            'vague' => $a2[0],
            'number' => $a2[1],
            'nativeCountry' => $a2[2],
        ];
    }

    public function configureDataStudentInformation($data, $file): HeStudentInformation
    {
        $studentInformation = new HeStudentInformation();

        $level = $this->heLevelRepository->findOneBy(['id' => $data['level']]);
        if ($level) {
            $studentInformation->setLevel($level);
        }
        $faculty = $this->heFacultyRepository->findOneById($data['faculty']);
        if ($faculty) {
            $studentInformation->setFaculty($faculty);
        }
        $studentInformation->setSexe($data['sexe']);
        $studentInformation->setNativeCountry($data['nativeCountry']);
        $studentInformation->setFormationType($data['formationType']);
        $studentInformation->setRegistrationFee(0);
        $studentInformation->setMonthlyFees(0);
        $studentInformation->setExaminationFees(0);
        $studentInformation->setDefenseFee(0);
        $studentInformation->setCertificateFee(0);

        // importation des fichiers




        // end of importation des fichiers
        $studentInformation->setType($data['type']);
        if ($data['type'] === "bachelor") {
            $studentInformation->setFmNames($data['fmNames']);
            $studentInformation->setFmJobs($data['fmJobs']);
            $studentInformation->setFmEmail($data['fmEmail']);
            $studentInformation->setFmPhoneNumber($data['fmPhoneNumber']);
            $studentInformation->setFmAdress($data['fmAdress']);
            $studentInformation->setTNames($data['tNames']);
            $studentInformation->setTJobs($data['tJobs']);
            $studentInformation->setTEmail($data['tEmail']);
            $studentInformation->setTPhoneNumber($data['tPhoneNumber']);
            $studentInformation->setTAdress($data['tAdress']);
        } else if ($data['type'] === "worker") {
            $studentInformation->setEmployerName($data['employerName']);
            $studentInformation->setJob($data['job']);
            $studentInformation->setGNames($data['gNames']);
            $studentInformation->setGEmail($data['gEmail']);
            $studentInformation->setGPhoneNumber($data['gPhoneNumber']);
        }

        // Les fichiers

        $cinPhoto = $file["cinPhoto"];
        $curriculumVitae = $file["curriculumVitae"];
        $noteOrCopyOfTheCertifiedDiploma = $file["noteOrCopyOfTheCertifiedDiploma"];
        $residenceCertificate = $file["residenceCertificate"];
        $birthCertificate = $file["birthCertificate"];
        $motivationLetter = $file["motivationLetter"];

        if ($cinPhoto) {
            $originalFilename = pathinfo($cinPhoto->getClientOriginalName(), PATHINFO_FILENAME);
            $safeFilename = $this->slugger->slug($originalFilename);
            $newFilename = $safeFilename . '-' . uniqid() . '.' . $cinPhoto->guessExtension();
            try {
                $cinPhoto->move(
                    $this->cinPhoto,
                    $newFilename
                );
            } catch (FileException $e) {
            }
            $studentInformation->setCinPhoto($newFilename);
        }

        if ($curriculumVitae) {
            $originalFilename = pathinfo($curriculumVitae->getClientOriginalName(), PATHINFO_FILENAME);
            $safeFilename = $this->slugger->slug($originalFilename);
            $newFilename = $safeFilename . '-' . uniqid() . '.' . $curriculumVitae->guessExtension();
            try {
                $curriculumVitae->move(
                    $this->curriculumVitae,
                    $newFilename
                );
            } catch (FileException $e) {
            }
            $studentInformation->setCurriculumVitae($newFilename);
        }

        if ($noteOrCopyOfTheCertifiedDiploma) {
            $originalFilename = pathinfo($noteOrCopyOfTheCertifiedDiploma->getClientOriginalName(), PATHINFO_FILENAME);
            $safeFilename = $this->slugger->slug($originalFilename);
            $newFilename = $safeFilename . '-' . uniqid() . '.' . $noteOrCopyOfTheCertifiedDiploma->guessExtension();
            try {
                $noteOrCopyOfTheCertifiedDiploma->move(
                    $this->noteOrCopyOfTheCertifiedDiploma,
                    $newFilename
                );
            } catch (FileException $e) {
            }
            $studentInformation->setNoteOrCopyOfTheCertifiedDiploma($newFilename);
        }

        if ($residenceCertificate) {
            $originalFilename = pathinfo($residenceCertificate->getClientOriginalName(), PATHINFO_FILENAME);
            $safeFilename = $this->slugger->slug($originalFilename);
            $newFilename = $safeFilename . '-' . uniqid() . '.' . $residenceCertificate->guessExtension();
            try {
                $residenceCertificate->move(
                    $this->residenceCertificate,
                    $newFilename
                );
            } catch (FileException $e) {
            }
            $studentInformation->setResidenceCertificate($newFilename);
        }

        if ($birthCertificate) {
            $originalFilename = pathinfo($birthCertificate->getClientOriginalName(), PATHINFO_FILENAME);
            $safeFilename = $this->slugger->slug($originalFilename);
            $newFilename = $safeFilename . '-' . uniqid() . '.' . $birthCertificate->guessExtension();
            try {
                $birthCertificate->move(
                    $this->birthCertificate,
                    $newFilename
                );
            } catch (FileException $e) {
            }
            $studentInformation->setBirthCertificate($newFilename);
        }

        if ($motivationLetter) {
            $originalFilename = pathinfo($motivationLetter->getClientOriginalName(), PATHINFO_FILENAME);
            $safeFilename = $this->slugger->slug($originalFilename);
            $newFilename = $safeFilename . '-' . uniqid() . '.' . $motivationLetter->guessExtension();
            try {
                $motivationLetter->move(
                    $this->motivationLetter,
                    $newFilename
                );
            } catch (FileException $e) {
            }
            $studentInformation->setMotivationLetter($newFilename);
        }

        return $studentInformation;
    }
}
