<?php

namespace App\Form;

use App\Entity\HeLevel;
use App\Entity\HeFaculty;
use App\Repository\HeLevelRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class RegistrationStudentFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder

            // COMMON USER
            ->add('lastName', TextType::class, [
                'label' => 'Nom',
                'attr' => [
                    'placeholder' => 'Votre nom',
                    'class' => 'form-control rsf_lastName'
                ]
            ])
            ->add('firstName', TextType::class, [
                'label' => 'Prénom',
                'attr' => [
                    'placeholder' => 'Votre prénom',
                    'class' => 'form-control rsf_firstName'
                ]
            ])
            ->add('email', EmailType::class, [
                'label' => 'Email',
                'required'  => true,
                'attr' => [
                    'placeholder' => 'Votre e-mail',
                    'class' => 'form-control rsf_email'
                ]
            ])
            ->add('phoneNumber', TelType::class, [
                'label' => 'Contact',
                'attr' => [
                    'placeholder' => 'Votre numero de téléphone',
                    'class' => 'form-control rsf_phoneNumber'
                ]
            ])
            ->add('birthDate', DateTimeType::class, [
                'label' => 'Date de naissance',
                'widget' => 'single_text',
                'attr' => [
                    'class' => 'form-control rsf_birthDate'
                ]
            ])
            ->add('birthPlace', TextType::class, [
                'label' => 'Lieu de naissance',
                'attr' => [
                    'placeholder' => 'Votre lieu de naissance',
                    'class' => 'form-control rsf_birthPlace'
                ]
            ])
            ->add('address', TextType::class, [
                'label' => 'Addresse',
                'attr' => [
                    'placeholder' => 'Votre addresse actuel',
                    'class' => 'form-control rsf_address'
                ]
            ])
            // ->add('imageFile', VichImageType::class, [
            //     'label' => 'Photo d\'identité',
            //     'required'  => true,
            //     'allow_delete'  => true,
            //     'download_label'  => 'Télécharger',
            //     'download_uri'  => true,
            //     'image_uri'  => true,
            //     'imagine_pattern'  => 'thumbnail_list',
            //     'asset_helper'  => true,
            //     'attr' => [
            //         'class' => 'form-control imageFileClass rsf_imageFile'
            //     ]
            // ])
           

            // STUDENT INFORMATION

            // ->add('matriculeNumber')
            ->add('employerName', TextType::class, [
                'label' => 'Nom de l\'employeur',
                'mapped' => false,
                'attr' => [
                    'placeholder' => 'Nom de l\'employeur',
                    'class' => 'form-control rsf_employerName'
                ]
            ])
            ->add('job', TextType::class, [
                'label' => 'Fonction',
                'mapped' => false,
                'attr' => [
                    'placeholder' => 'Votre fonction',
                    'class' => 'form-control rsf_job'
                ]
            ])
            ->add(
                'fmNames',
                TextType::class,
                [
                    'label' => 'Nom et prénom(s) du père ou de la mère',
                    'mapped' => false,
                    'attr' => [
                        'placeholder' => 'Nom et prénom du père ou de la mère',
                        'class' => 'form-control rsf_fmNames'
                    ]
                ]
            )
            ->add('fmJobs', TextType::class, [
                'label' => 'Fonction',
                'mapped' => false,
                'attr' => [
                    'placeholder' => 'Fonction',
                    'class' => 'form-control rsf_fmJobs'
                ]
            ])
            ->add('fmEmail', EmailType::class, [
                'label' => 'E-mail',
                'mapped' => false,
                'attr' => [
                    'placeholder' => 'E-mail',
                    'class' => 'form-control rsf_fmEmail'
                ]
            ])
            ->add('fmPhoneNumber', TelType::class, [
                'label' => 'Contact',
                'mapped' => false,
                'attr' => [
                    'placeholder' => 'Contact',
                    'class' => 'form-control rsf_fmPhoneNumber'
                ]
            ])
            ->add('fmAdress', TextType::class, [
                'label' => 'Adresse',
                'mapped' => false,
                'attr' => [
                    'placeholder' => 'Adresse',
                    'class' => 'form-control rsf_fmAdress'
                ]
            ])
            ->add('tNames', TextType::class, [
                'label' => 'Nom et prénom(s) du tuteur ou de la tutrice',
                'mapped' => false,
                'attr' => [
                    'placeholder' => 'nom et prénom(s) du tuteur ou de la tutrice',
                    'class' => 'form-control rsf_tNames'
                ]
            ])
            ->add('tJobs', TextType::class, [
                'label' => 'Fonction',
                'mapped' => false,
                'attr' => [
                    'placeholder' => 'fonction',
                    'class' => 'form-control rsf_tJobs'
                ]
            ])
            ->add('tEmail', EmailType::class, [
                'label' => 'E-mail',
                'mapped' => false,
                'attr' => [
                    'placeholder' => 'e-mail',
                    'class' => 'form-control rsf_tEmail'
                ]
            ])
            ->add('tPhoneNumber', TelType::class, [
                'label' => 'Contact',
                'mapped' => false,
                'attr' => [
                    'placeholder' => 'contact',
                    'class' => 'form-control rsf_tPhoneNumber'
                ]
            ])
            ->add('tAdress', TextType::class, [
                'label' => 'Adresse',
                'mapped' => false,
                'attr' => [
                    'placeholder' => 'adresse',
                    'class' => 'form-control rsf_tAdress'
                ]
            ])
            ->add('type', ChoiceType::class, [
                'label' => 'Êtes-vous?',
                'mapped' => false,
                'choices' => [
                    "Bachelier/ère" => "bachelor",
                    "travailleur(euse)" => "worker"
                ],
                'attr' => [
                    'class' => 'form-control rsf_type'
                ]
            ])
            ->add('gNames', TextType::class, [
                'label' => 'Nom du garant',
                'mapped' => false,
                'attr' => [
                    'placeholder' => 'nom du garant',
                    'class' => 'form-control rsf_gNames'
                ]
            ])
            ->add('gEmail', EmailType::class, [
                'label' => 'E-mail',
                'mapped' => false,
                'attr' => [
                    'placeholder' => 'e-mail',
                    'class' => 'form-control rsf_gEmail'
                ]
            ])
            ->add('gPhoneNumber', TelType::class, [
                'label' => 'Contact',
                'mapped' => false,
                'attr' => [
                    'placeholder' => 'contact',
                    'class' => 'form-control rsf_gPhoneNumber'
                ]
            ])
          
            // ->add('faculty', EntityType::class, [
            //     'label' => 'La fonction RH dans l’entreprise',
            //     'mapped' => false,
            //     'class' => HeFaculty::class,
            //     'choice_label' => 'name',
            //     'attr' => [
            //         'class' => 'form-control rsf_level'
            //     ]
            // ])
            ->add('formationType', ChoiceType::class, [
                'label' => 'La fonction RH dans l’entreprise',
                'mapped' => false,
                'choices' => [
                    "Organisation de l’entreprise" => "Organisation de l’entreprise",
                    "La gestion du changement" => "La gestion du changement",
                    "Développer la qualité de service de la fonction RH"=>"Développer la qualité de service de la fonction RH"
                ],
                'attr' => [
                    'class' => 'form-control rsf_formationType'
                ]
            ])
            ->add('formationType1', ChoiceType::class, [
                'label' => 'Gérer et développer les compétences ',
                'mapped' => false,
                'choices' => [
                    "Politique de recrutement" => "Politique de recrutement",
                    "Formation" => "Formation",
                    "Organisation du travail"=>"Organisation du travail"
                ],
                'attr' => [
                    'class' => 'form-control rsf_formationType1'
                ]
            ])
            ->add('formationType2', ChoiceType::class, [
                'label' => 'Initiation à la gestion industrielle',
                'mapped' => false,
                'choices' => [
                    "Les différentes fonctions de l’entreprise : rôle et interdépendance" => "Les différentes fonctions de l’entreprise : rôle et interdépendance",
                    "Les organisations de travail : service client, flexibilité, flux et cycle" => "Les organisations de travail : service client, flexibilité, flux et cycle",
                    "L’organisation de la production et les outils utilisés"=>"L’organisation de la production et les outils utilisés"
                ],
                'attr' => [
                    'class' => 'form-control rsf_formationType2'
                ]
            ])
            ->add('formationType3', ChoiceType::class, [
                'label' => 'Qualité et prévention',
                'mapped' => false,
                'choices' => [
                    "La qualité, but et enjeux, principe du management de la qualité, non-conformité, rôle qualité" => "La qualité, but et enjeux, principe du management de la qualité, non-conformité, rôle qualité",
                    "Le système de management de la qualité : l’approche processus, le système documentaire" => "Le système de management de la qualité : l’approche processus, le système documentaire",
                    "Les notions de législation et de règlementation en matière de sécurité et de condition de travail"=>"Les notions de législation et de règlementation en matière de sécurité et de condition de travail",
                    "L’obligation d’évaluation des risques pour la sécurité et la santé des salariés"=>"L’obligation d’évaluation des risques pour la sécurité et la santé des salariés",
                    "Organisation de la prévention dans l’entreprise"=>"Organisation de la prévention dans l’entreprise"
                ],
                'attr' => [
                    'class' => 'form-control rsf_formationType3'
                ]
            ])
            ->add('formationType4', ChoiceType::class, [
                'label' => 'Communication orale et écrite',
                'mapped' => false,
                'choices' => [
                    "Communication et prise de parole" => "Communication et prise de parole",
                    "Prise de note, compte rendu" => "Prise de note, compte rendu"
                ],
                'attr' => [
                    'class' => 'form-control rsf_formationType4'
                ]
            ])
            ->add('formationType5', ChoiceType::class, [
                'label' => 'Savoir communiquer et travailler dans une équipe multidisciplinaire',
                'mapped' => false,
                'choices' => [
                    "Sensibilisation à la communication et à l’information" => "Sensibilisation à la communication et à l’information",
                    "Connaissance de son style et des autres styles pour adapter sa communication" => "Connaissance de son style et des autres styles pour adapter sa communication",
                    "Les difficultés de la communication"=>"Les difficultés de la communication",
                    "L’écoute et le questionnement"=>"L’écoute et le questionnement",
                    "L’empathie dans la communication, savoir donner son avis, savoir faire une proposition et l’argumenter, le processus de résolution"=>"L’empathie dans la communication, savoir donner son avis, savoir faire une proposition et l’argumenter, le processus de résolution"
                ],
                'attr' => [
                    'class' => 'form-control rsf_formationType5'
                ]
            ])
            ->add('formationType6', ChoiceType::class, [
                'label' => 'S’affirmer dans ses relations professionnelles',
                'mapped' => false,
                'choices' => [
                    "Développer la confiance en soi" => "Développer la confiance en soi",
                    "Créer un système de croyance positive" => "Créer un système de croyance positive",
                    "Comprendre la communication"=>"Comprendre la communication",
                    "La gestuelle"=>"La gestuelle",
                    "Comment s’affirmer dans une relation professionnelle"=>"Comment s’affirmer dans une relation professionnelle"
                ],
                'attr' => [
                    'class' => 'form-control rsf_formationType6'
                ]
            ])
            ->add('formationType7', ChoiceType::class, [
                'label' => 'Animation d’un groupe de projet/groupe de travail',
                'mapped' => false,
                'choices' => [
                    "La préparation de la réunion" => "La préparation de la réunion",
                    "La prise en compte des personnalités savoir animer un groupe" => "La prise en compte des personnalités savoir animer un groupe",
                    "Gérer les blocages"=>"Gérer les blocages",
                    "Conclure une réunion"=>"Conclure une réunion"
                ],
                'attr' => [
                    'class' => 'form-control rsf_formationType7'
                ]
            ])
            ->add('formationType8', ChoiceType::class, [
                'label' => 'Droit du travail et relation sociale',
                'mapped' => false,
                'choices' => [
                    "Connaissance de la législation sociale (les bases juridiques du droit du travail, la hiérarchie des normes en droit de travail)" => "Connaissance de la législation sociale (les bases juridiques du droit du travail, la hiérarchie des normes en droit de travail)",
                    "Relations sociales et négociations sociales" => "Relations sociales et négociations sociales",
                    "Cadre social de l’entreprise"=>"Cadre social de l’entreprise",
                ],
                'attr' => [
                    'class' => 'form-control rsf_formationType8'
                ]
            ])
            ->add('formationType9', ChoiceType::class, [
                'label' => 'Paie et politique de rémunération',
                'mapped' => false,
                'choices' => [
                    "Les bases de la paie" => "Les bases de la paie",
                    "Pilotage de masse salariale" => "Pilotage de masse salariale",
                    "Les différentes composantes de la rémunération et la rémunération globale"=>"Les différentes composantes de la rémunération et la rémunération globale"
                ],
                'attr' => [
                    'class' => 'form-control rsf_formationType9'
                ]
            ])
            ->add('formationType10', ChoiceType::class, [
                'label' => 'Prévention des risques professionnels',
                'mapped' => false,
                'choices' => [
                    "Santé, sécurité au travail" => "Santé, sécurité au travail",
                    "Traitement de l’absentéisme" => "Traitement de l’absentéisme"
                ],
                'attr' => [
                    'class' => 'form-control rsf_formationType10'
                ]
            ])
            ->add('formationType11', ChoiceType::class, [
                'label' => 'Les outils de la résolution de problème et amélioration continue',
                'mapped' => false,
                'choices' => [
                    "Comprendre le problème" => "Comprendre le problème",
                    "Collecter l’information" => "Collecter l’information",
                    "Développer les idées"=>"Développer les idées"
                ],
                'attr' => [
                    'class' => 'form-control rsf_formationType11'
                ]
            ])
            ->add('formationType12', ChoiceType::class, [
                'label' => 'Gestion du personnel (méthode et action)',
                'mapped' => false,
                'choices' => [
                    "Situer le contexte juridique de la gestion du personnel" => "Situer le contexte juridique de la gestion du personnel",
                    "Mettre en place une organisation/gestion efficace" => "Mettre en place une organisation/gestion efficace",
                    "Développer les idées"=>"Développer les idées"
                ],
                'attr' => [
                    'class' => 'form-control rsf_formationType12'
                ]
            ])
            ->add('formationType13', ChoiceType::class, [
                'label' => 'Gérer une situation conflictuelle',
                'mapped' => false,
                'choices' => [
                    "Définir la tension et le conflit" => "Définir la tension et le conflit",
                    "Comprendre les mécanismes et raison du conflit" => "Comprendre les mécanismes et raison du conflit",
                    "Reconnaitre et canaliser son comportement"=>"Reconnaitre et canaliser son comportement",
                    "Les mécanismes du stress"=>"Les mécanismes du stress"
                ],
                'attr' => [
                    'class' => 'form-control rsf_formationType13'
                ]
            ])
            ->add('formationType14', ChoiceType::class, [
                'label' => 'Mieux gérer son temps et ses priorités',
                'mapped' => false,
                'choices' => [
                    "La réflexion sur le temps" => "La réflexion sur le temps",
                    "Chasser les voleurs de temps" => "Chasser les voleurs de temps",
                    "Définir ses objectifs"=>"Définir ses objectifs",
                    "Détermination des priorités"=>"Détermination des priorités",
                    "Savoir déléguer"=>"Savoir déléguer"
                ],
                'attr' => [
                    'class' => 'form-control rsf_formationType14'
                ]
            ])
            // NEW

           


            ->add('sexe', ChoiceType::class, [
                'label' => 'Sexe',
                'mapped' => false,
                'choices' => [
                    "Masculin" => "male",
                    "Feminin" => "feminine"
                ],
                'attr' => [
                    'class' => 'form-control rsf_sexe'
                ]
            ])

            ->add('nativeCountry', CountryType::class, [
                'label' => 'Pays d\'origine',
                'mapped' => false,
            ])

            ->add('cinPhoto', FileType::class, [
                'label' => 'CIN *',
                'mapped' => false,
                'required' => true,
                'attr' => [
                    'class' => 'form-control rsf_cinPhoto'
                ],
                'constraints' => [
                    new File([
                        'maxSize' => '10240k',
                        'maxSizeMessage' => 'Veuillez télécharger un document PDF valide',
                    ]),
                    new NotBlank([
                        'message'  => 'Il faut le remplir'
                    ])
                ],
            ])

            ->add('curriculumVitae', FileType::class, [
                'label' => 'Curriculum vitae *',
                'mapped' => false,
                'required' => true,
                'attr' => [
                    'class' => 'form-control rsf_curriculumVitae'
                ],
                'constraints' => [
                    new File([
                        'maxSize' => '10240k',
                        'maxSizeMessage' => 'Veuillez télécharger un document PDF valide',
                    ]),
                    new NotBlank([
                        'message'  => 'Il faut le remplir'
                    ])

                ],
            ])

            // ->add('noteOrCopyOfTheCertifiedDiploma', FileType::class, [
            //     'label' => 'Note ou copie du diplome certifié *',
            //     'mapped' => false,
            //     'required' => true,
            //     'attr' => [
            //         'class' => 'form-control rsf_noteOrCopyOfTheCertifiedDiploma'
            //     ],
            //     'constraints' => [
            //         new File([
            //             'maxSize' => '10240k',
            //             'maxSizeMessage' => 'Veuillez télécharger un document PDF valide',
            //         ]),
            //         new NotBlank([
            //             'message'  => 'Il faut le remplir'
            //         ])
            //     ],
            // ])

            // ->add('residenceCertificate', FileType::class, [
            //     'label' => 'Certificat de residance *',
            //     'mapped' => false,
            //     'required' => true,
            //     'attr' => [
            //         'class' => 'form-control rsf_residenceCertificate'
            //     ],
            //     'constraints' => [
            //         new File([
            //             'maxSize' => '10240k',
            //             'maxSizeMessage' => 'Veuillez télécharger un document PDF valide',
            //         ]),
            //         new NotBlank([
            //             'message'  => 'Il faut le remplir'
            //         ])
            //     ],
            // ])

            // ->add('birthCertificate', FileType::class, [
            //     'label' => 'Buillet de naissace *',
            //     'mapped' => false,
            //     'required' => true,
            //     'attr' => [
            //         'class' => 'form-control rsf_birthCertificate'
            //     ],
            //     'constraints' => [
            //         new File([
            //             'maxSize' => '10240k',
            //             'maxSizeMessage' => 'Veuillez télécharger un document PDF valide',
            //         ]),
            //         new NotBlank([
            //             'message'  => 'Il faut le remplir'
            //         ])
            //     ],
            // ])


            ->add('motivationLetter', FileType::class, [
                'label' => 'Lettre de motivation *',
                'mapped' => false,
                'required' => true,
                'attr' => [
                    'class' => 'form-control rsf_motivationLetter'
                ],
                'constraints' => [
                    new File([
                        'maxSize' => '10240k',
                        'maxSizeMessage' => 'Veuillez télécharger un document PDF valide',
                    ]),
                    new NotBlank([
                        'message'  => 'Il faut le remplir'
                    ])
                ],
            ])




            //
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
