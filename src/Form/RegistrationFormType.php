<?php

namespace App\Form;

use App\Entity\HeUser;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('lastName', null, [
                'label' => 'Nom',
                'required'  => true,
                'attr' => [
                    'placeholder' => 'Votre nom',
                    'class' => 'form-control'
                ]
            ])
            ->add('firstName', null, [
                'label' => 'Prénom',
                'required'  => true,
                'attr' => [
                    'placeholder' => 'Votre prénom',
                    'class' => 'form-control'
                ]
            ])
            ->add('email', null, [
                'label' => 'Email',
                'required'  => true,
                'attr' => [
                    'placeholder' => 'Votre e-mail',
                    'class' => 'form-control'
                ]
            ])
            ->add('birthDate', DateTimeType::class, [
                'label' => 'Date de naissance',
                'widget' => 'single_text',
                'required'  => true,
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('birthPlace', null, [
                'label' => 'Lieu de naissance',
                'required'  => true,
                'attr' => [
                    'placeholder' => 'Votre lieu de naissance',
                    'class' => 'form-control'
                ]
            ])
            ->add('address', null, [
                'label' => 'Addresse',
                'required'  => true,
                'attr' => [
                    'placeholder' => 'Votre addresse actuel',
                    'class' => 'form-control'
                ]
            ])
            ->add('imageFile', VichImageType::class, [
                'label' => 'Votre image',
                'required'  => true,
                'allow_delete'  => true,
                'download_label'  => 'Télécharger',
                'download_uri'  => true,
                'image_uri'  => true,
                'imagine_pattern'  => 'thumbnail_list',
                'asset_helper'  => true,
                'attr' => [
                    'class' => 'form-control imageFileClass'
                ]
            ])
            ->add('agreeTerms', CheckboxType::class, [
                'mapped' => false,
                'label' => "Accepter les conditions",
                'constraints' => [
                    new IsTrue([
                        'message' => 'Vous devez accepter nos conditions.',
                    ]),
                ],
            ])
            ->add('plainPassword', PasswordType::class, [
                'mapped' => false,
                'label' => 'Votre mot de passe',
                'attr' => ['autocomplete' => 'new-password', 'class' => 'form-control', 'placeholder' => 'Votre mot de passe'],
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez entrer un mot de passe',
                    ]),
                    new Length([
                        'min' => 6,
                        'minMessage' => 'Votre mot de passe doit comporter au moins {{ limit }} caractères',
                        'max' => 4096,
                    ]),
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => HeUser::class,
        ]);
    }
}
