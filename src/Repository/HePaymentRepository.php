<?php

namespace App\Repository;

use App\Entity\HePayment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<HePayment>
 *
 * @method HePayment|null find($id, $lockMode = null, $lockVersion = null)
 * @method HePayment|null findOneBy(array $criteria, array $orderBy = null)
 * @method HePayment[]    findAll()
 * @method HePayment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HePaymentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, HePayment::class);
    }

    public function add(HePayment $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(HePayment $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @return HePayment[] Returns an array of HePayment objects
     */
    public function getRequestUnReadByType($type): array
    {
        return $this->createQueryBuilder('hep')
            ->andWhere("hep.type = :type AND hep.state = 'unread'")
            ->setParameter('type', $type)
            ->orderBy('hep.id', 'ASC')
            ->getQuery()
            ->getResult();
    }

    //    public function findOneBySomeField($value): ?IePayment
    //    {
    //        return $this->createQueryBuilder('i')
    //            ->andWhere('i.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
