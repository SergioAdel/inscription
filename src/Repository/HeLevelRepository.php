<?php

namespace App\Repository;

use App\Entity\HeLevel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<HeLevel>
 *
 * @method HeLevel|null find($id, $lockMode = null, $lockVersion = null)
 * @method HeLevel|null findOneBy(array $criteria, array $orderBy = null)
 * @method HeLevel[]    findAll()
 * @method HeLevel[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HeLevelRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, HeLevel::class);
    }

    public function add(HeLevel $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(HeLevel $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @return HeLevel[] Returns an array of HeLevel objects
     */
    public function getLevelForRegistrationFront()
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.name != :name')
            ->setParameter('name', "Licence 3")
            ->orderBy('h.name', 'ASC');
    }

    //    public function findOneBySomeField($value): ?IeLevel
    //    {
    //        return $this->createQueryBuilder('i')
    //            ->andWhere('i.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
