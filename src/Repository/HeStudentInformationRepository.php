<?php

namespace App\Repository;

use App\Entity\HeFaculty;
use App\Entity\HeLevel;
use App\Entity\HeStudentInformation;
use App\Entity\HeUser;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<HeStudentInformation>
 *
 * @method HeStudentInformation|null find($id, $lockMode = null, $lockVersion = null)
 * @method HeStudentInformation|null findOneBy(array $criteria, array $orderBy = null)
 * @method HeStudentInformation[]    findAll()
 * @method HeStudentInformation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HeStudentInformationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, HeStudentInformation::class);
    }

    public function add(HeStudentInformation $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(HeStudentInformation $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @return HeStudentInformation[] Returns an array of HeStudentInformation objects
     */
    public function getMatriculeNumberByFaculty(HeFaculty $heFaculty): array
    {
        return $this->createQueryBuilder('si')
            ->innerJoin('si.faculty', 'f', 'WITH', 'f = :faculty')
            ->innerJoin('si.users', 'u')
            ->andWhere('u.isDeleted = false AND u.confirmAt IS NOT NULL')
            ->andWhere('si.matriculeNumber IS NOT NULL')
            ->setParameter('faculty', $heFaculty)
            ->orderBy('si.id', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return HeStudentInformation[] Returns an array of HeStudentInformation objects
     */
    public function getNumberStudentOfFaculty(HeFaculty $heFaculty): array
    {
        return $this->createQueryBuilder('si')
            ->innerJoin('si.faculty', 'f', 'WITH', 'f = :faculty')
            ->innerJoin('si.users', 'u')
            ->andWhere('u.isDeleted = false AND u.confirmAt IS NOT NULL')
            ->andWhere('si.matriculeNumber IS NOT NULL')
            ->setParameter('faculty', $heFaculty)
            ->orderBy('si.id', 'ASC')
            ->getQuery()
            ->getResult();
    }


    /**
     * @return HeStudentInformation[] Returns an array of HeStudentInformation objects
     */
    public function getStudentByFormationTypeOfFaculty(HeFaculty $heFaculty, string $formationType): array
    {
        return $this->createQueryBuilder('si')
            ->innerJoin('si.faculty', 'f', 'WITH', 'f = :faculty')
            ->innerJoin('si.users', 'u')
            ->andWhere('u.isDeleted = false AND u.confirmAt IS NOT NULL')
            ->andWhere('si.matriculeNumber IS NOT NULL')
            ->setParameter('faculty', $heFaculty)
            ->andWhere('si.formationType = :formationType')
            ->setParameter('formationType', $formationType)
            ->orderBy('si.id', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return HeStudentInformation[] Returns an array of HeStudentInformation objects
     */
    public function getStudentMadagascarOfFaculty(HeFaculty $heFaculty): array
    {
        return $this->createQueryBuilder('si')
            ->innerJoin('si.faculty', 'f', 'WITH', 'f = :faculty')
            ->innerJoin('si.users', 'u')
            ->andWhere('u.isDeleted = false AND u.confirmAt IS NOT NULL')
            ->andWhere('si.matriculeNumber IS NOT NULL')
            ->setParameter('faculty', $heFaculty)
            ->andWhere('si.nativeCountry = :nat')
            ->setParameter('nat', 'MG')
            ->orderBy('si.id', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return HeStudentInformation[] Returns an array of HeStudentInformation objects
     */
    public function getStudentAbroadOfFaculty(HeFaculty $heFaculty): array
    {
        return $this->createQueryBuilder('si')
            ->innerJoin('si.faculty', 'f', 'WITH', 'f = :faculty')
            ->innerJoin('si.users', 'u')
            ->andWhere('u.isDeleted = false AND u.confirmAt IS NOT NULL')
            ->andWhere('si.matriculeNumber IS NOT NULL')
            ->setParameter('faculty', $heFaculty)
            ->andWhere('si.nativeCountry != :nat')
            ->setParameter('nat', 'MG')
            ->orderBy('si.id', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return HeStudentInformation[] Returns an array of HeStudentInformation objects
     */
    public function getStudentByLevelOfFaculty(HeFaculty $heFaculty, string $level): array
    {
        return $this->createQueryBuilder('si')
            ->innerJoin('si.faculty', 'f', 'WITH', 'f = :faculty')
            ->innerJoin('si.level', 'l', 'WITH', 'l.name = :level')
            ->innerJoin('si.users', 'u')
            ->andWhere('u.isDeleted = false AND u.confirmAt IS NOT NULL')
            ->andWhere('si.matriculeNumber IS NOT NULL')
            ->setParameter('faculty', $heFaculty)
            ->setParameter('level', $level)
            ->orderBy('si.id', 'ASC')
            ->getQuery()
            ->getResult();
    }


    public function getInformationByUser(HeUser $heUser): ?HeStudentInformation
    {
        return $this->createQueryBuilder('a')
            ->innerJoin('a.users', 'u', 'WITH', 'u.id = :user')
            ->setParameter('user', $heUser->getId())
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @return HeStudentInformation[] Returns an array of HeStudentInformation objects
     */
    public function getRegistratedStudentByFaculty(HeFaculty $heFaculty): array
    {
        return $this->createQueryBuilder('si')
            ->innerJoin('si.faculty', 'f', 'WITH', 'f = :faculty')
            ->innerJoin('si.users', 'u')
            ->setParameter('faculty', $heFaculty)
            ->andWhere('u.isDeleted = false AND u.confirmAt IS NOT NULL')
            ->andWhere('si.matriculeNumber IS NOT NULL')
            ->orderBy('si.id', 'ASC')
            ->getQuery()
            ->getResult();
    }


    /**
     * @return HeStudentInformation[] Returns an array of HeStudentInformation objects
     */
    public function getFilterStudentInformation(HeStudentInformation $heStudentInformation): array
    {
        // dd($ieStudentInformation);
        $queryB = $this->createQueryBuilder('si')
            ->innerJoin('si.users', 'u')
            ->innerJoin('si.faculty', 'f', 'WITH', 'f = :faculty')
            ->setParameter('faculty', $heStudentInformation->getFaculty());

        if ($heStudentInformation->getType()) $queryB->andWhere('si.formationType = :type')->setParameter('type', $heStudentInformation->getType());
        if ($heStudentInformation->getLevel()) $queryB->innerJoin('si.level', 'l', 'WITH', 'l = :level')->setParameter('level', $heStudentInformation->getLevel());
        if ($heStudentInformation->getSemester()) $queryB->innerJoin('si.semester', 's', 'WITH', 's = :semester')->setParameter('semester', $heStudentInformation->getSemester());
        if ($heStudentInformation->getVague()) $queryB->innerJoin('si.vague', 'v', 'WITH', 'v = :vague')->setParameter('vague', $heStudentInformation->getVague());

        return $queryB
            ->andWhere('u.isDeleted = false AND u.confirmAt IS NOT NULL')
            ->andWhere('si.matriculeNumber IS NOT NULL')
            ->orderBy('si.id', 'ASC')
            ->getQuery()
            ->getResult();
    }


    /**
     * @return HeStudentInformation[] Returns an array of HeStudentInformation objects
     */
    public function getPaymentStatusStudent(HeStudentInformation $heStudentInformation): array
    {
        // dd($ieStudentInformation);
        $queryB = $this->createQueryBuilder('si')
            ->innerJoin('si.users', 'u');

        if ($heStudentInformation->getType()) $queryB->andWhere('si.formationType = :type')->setParameter('type', $heStudentInformation->getType());
        if ($heStudentInformation->getLevel()) $queryB->innerJoin('si.level', 'l', 'WITH', 'l = :level')->setParameter('level', $heStudentInformation->getLevel());
        if ($heStudentInformation->getFaculty()) $queryB->innerJoin('si.faculty', 'f', 'WITH', 'f = :faculty')->setParameter('faculty', $heStudentInformation->getFaculty());
        if ($heStudentInformation->getSemester()) $queryB->innerJoin('si.semester', 's', 'WITH', 's = :semester')->setParameter('semester', $heStudentInformation->getSemester());
        if ($heStudentInformation->getVague()) $queryB->innerJoin('si.vague', 'v', 'WITH', 'v = :vague')->setParameter('vague', $heStudentInformation->getVague());

        return $queryB
            ->andWhere('u.isDeleted = false AND u.confirmAt IS NOT NULL')
            ->andWhere('si.matriculeNumber IS NOT NULL')
            ->orderBy('si.id', 'ASC')
            ->getQuery()
            ->getResult();
    }
}