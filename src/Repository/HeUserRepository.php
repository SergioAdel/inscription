<?php

namespace App\Repository;

use App\Entity\HeUser;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;

/**
 * @extends ServiceEntityRepository<HeUser>
 *
 * @method HeUser|null find($id, $lockMode = null, $lockVersion = null)
 * @method HeUser|null findOneBy(array $criteria, array $orderBy = null)
 * @method HeUser[]    findAll()
 * @method HeUser[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HeUserRepository extends ServiceEntityRepository implements PasswordUpgraderInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, HeUser::class);
    }

    public function add(HeUser $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(HeUser $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function upgradePassword(PasswordAuthenticatedUserInterface $user, string $newHashedPassword): void
    {
        if (!$user instanceof HeUser) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', \get_class($user)));
        }

        $user->setPassword($newHashedPassword);

        $this->add($user, true);
    }

    /**
     * @return HeUser[] Returns an array of HeUser objects
     */
    public function getUserByRoleType($name): array
    {
        return $this->createQueryBuilder('heu')
            ->innerJoin('heu.heRoles', 'her', 'WITH', 'herr.name = :name')
            ->setParameter('name', $name)
            ->andWhere('heu.isDeleted = 0')
            ->orderBy('heu.id', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return HeUser[] Returns an array of HeUser objects
     */
    public function getUserByRoleTypeNonConfirm($name): array
    {
        return $this->createQueryBuilder('heu')
            ->innerJoin('heu.heRoles', 'her', 'WITH', 'her.name = :name')
            ->setParameter('name', $name)
            ->andWhere('heu.isDeleted = 0')
            ->andWhere('heu.confirmAt IS NULL')
            ->orderBy('heu.id', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return HeUser[] Returns an array of HeUser objects
     */
    public function getUserByRoleTypeNonVerified($name): array
    {
        return $this->createQueryBuilder('heu')
            ->innerJoin('heu.heRoles', 'her', 'WITH', 'her.name = :name')
            ->setParameter('name', $name)
            ->andWhere('heu.isDeleted = 0')
            ->andWhere('heu.isVerified = 0')
            ->orderBy('heu.id', 'ASC')
            ->getQuery()
            ->getResult();
    }

    //    public function findOneBySomeField($value): ?IeUser
    //    {
    //        return $this->createQueryBuilder('i')
    //            ->andWhere('i.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
