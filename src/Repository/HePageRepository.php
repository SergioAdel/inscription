<?php

namespace App\Repository;

use App\Entity\HePage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<HePage>
 *
 * @method HePage|null find($id, $lockMode = null, $lockVersion = null)
 * @method HePage|null findOneBy(array $criteria, array $orderBy = null)
 * @method HePage[]    findAll()
 * @method HePage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HePageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, HePage::class);
    }

    public function add(HePage $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(HePage $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @return HePage[] Returns an array of HePage objects
     */
    public function findNotDeletedPage(): array
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.isDeleted = 0')
            ->orderBy('h.menuOrder', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return HePage[] Returns an array of HePage objects
     */
    public function findNotDeletedPageOn(): array
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.isDeleted = 0')
            ->andWhere('h.isActive = 1')
            ->orderBy('h.menuOrder', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return HePage[] Returns an array of HePage objects
     */
    public function findNotDeletedPageOff(): array
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.isDeleted = 0')
            ->andWhere('h.isActive = 0')
            ->orderBy('h.menuOrder', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return HePage[] Returns an array of HePage objects
     */
    public function getPages(): array
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.isDeleted = 0')
            ->andWhere('h.isActive = 1')
            ->orderBy('h.menuOrder', 'ASC')
            ->getQuery()
            ->getResult();
    }

    //    public function findOneBySomeField($value): ?IePage
    //    {
    //        return $this->createQueryBuilder('i')
    //            ->andWhere('i.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
