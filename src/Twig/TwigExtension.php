<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class TwigExtension extends AbstractExtension
{

    public function getFunctions(): array
    {
        return [
            new TwigFunction('convert', [$this, 'convert']),
            new TwigFunction('renderIcon', [$this, 'renderIcon']),
            new TwigFunction('getFormationType', [$this, 'getFormationType']),
        ];
    }

    public function convert($value)
    {
        return [
            'ROLE_STUDENT' => 'Étudiant',
            'ROLE_ADMIN' => 'Administrateur',
            'ROLE_TEACHER' => "Proffesseur",
            'ROLE_SCHOOL_MANAGER' => "Géstionnaire d'écolage"
        ][$value];
    }

    public function getFormationType($formationType)
    {
        return [
            'face-to-face' => 'Formation en présentiel',
            'online' => 'Formation en ligne',
        ][$formationType];
    }


    public function renderIcon($value)
    {
        return [
            'ROLE_STUDENT' => 'fa-graduation-cap',
            'ROLE_ADMIN' => 'fa-screwdriver',
            'ROLE_TEACHER' => "fa-chalkboard-teacher",
            'ROLE_SCHOOL_MANAGER' => "fa-credit-card"
        ][$value];
    }
}
