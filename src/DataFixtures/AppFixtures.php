<?php

namespace App\DataFixtures;

use App\Entity\HeDegree;
use App\Entity\HeFaculty;
use App\Entity\HeLevel;
use App\Entity\HeRole;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {

        // ROLES

        $roleTypes = [
            "ROLE_STUDENT" => "Étudiant",
            "ROLE_ADMIN" => "Administrateur",
            "ROLE_TEACHER" => "Proffesseur",
            "ROLE_SCHOOL_MANAGER" => "Géstionnaire d'écolage"
        ];

        $roles = [];
        foreach ($roleTypes as $name => $description) {
            $heRoles = new HeRole();
            $heRoles->setName($name)->setDescription($description);
            $roles[] = $heRoles;
            $manager->persist($heRoles);
        }

        // DIPLOME

        $degreeTypes = [
            "La fonction RH dans l’entreprise" => "La fonction RH dans l’entreprise",
            "Gérer et développer les compétences" => "Gérer et développer les compétences",
            "Initiation à la gestion industrielle" => "Initiation à la gestion industrielle",
            "Qualité et prévention"=>"Qualité et prévention",
            "Communication orale et écrite"=>"Communication orale et écrite",
            "Savoir communiquer et travailler dans une équipe multidisciplinaire"=>"Savoir communiquer et travailler dans une équipe multidisciplinaire",
            "S’affirmer dans ses relations professionnelles"=>"S’affirmer dans ses relations professionnelles",
            "Animation d’un groupe de projet/groupe de travail"=>"Animation d’un groupe de projet/groupe de travail",
            "Droit du travail et relation sociale"=>"Droit du travail et relation sociale",
            "Paie et politique de rémunération"=>"Paie et politique de rémunération",
            "Prévention des risques professionnels"=>"Prévention des risques professionnels",
            "Les outils de la résolution de problème et amélioration continue"=>"Les outils de la résolution de problème et amélioration continue",
            "Gestion du personnel"=>"Gestion du personnel",
            "Gérer une situation conflictuelle"=>"Gérer une situation conflictuelle",
            "Mieux gérer son temps et ses priorités"=>"Mieux gérer son temps et ses priorités",

        ];

        $degrees = [];
        foreach ($degreeTypes as $name => $description) {
            $heDegree = new HeDegree();
            $heDegree->setName($name)->setDescription($description);
            $degrees[] = $heDegree;
            $manager->persist($heDegree);
        }

        // NIVEAU

        $levelTypes = [
            "Licence 1" => "Licence 1",
            "Licence 2" => "Licence 2",
            "Licence 3" => "Licence 3",
            "Master 1" => "Master 1",
            "Master 2" => "Master 2"
        ];

        $levels = [];
        $counterDegree = 0;
        foreach ($levelTypes as $name => $description) {
            $heLevel = new HeLevel();
            $heLevel->setName($name)->setDescription($description);

            if ($counterDegree < 3) {
                $heLevel->setDegree($degrees[0]);
            } else {
                $heLevel->setDegree($degrees[1]);
            }
            $counterDegree++;
            $levels[] = $heLevel;
            $manager->persist($heLevel);
        }


        // FILIÈRE

        $facultyTypes = [
            "La fonction RH dans l’entreprise" => "Gérer et développer les compétences",
            "Gérer et développer les compétences" => "Gérer et développer les compétences",
            "Initiation à la gestion industrielle" => "Initiation à la gestion industrielle",
            "Qualité et prévention"=>"Qualité et prévention",
            "Communication orale et écrite"=>"Communication orale et écrite",
            "Savoir communiquer et travailler dans une équipe multidisciplinaire"=>"Savoir communiquer et travailler dans une équipe multidisciplinaire",
            "S’affirmer dans ses relations professionnelles"=>"S’affirmer dans ses relations professionnelles",
            "Animation d’un groupe de projet/groupe de travail"=>"Animation d’un groupe de projet/groupe de travail",
            "Droit du travail et relation sociale"=>"Droit du travail et relation sociale",
            "Paie et politique de rémunération"=>"Paie et politique de rémunération",
            "Prévention des risques professionnels"=>"Prévention des risques professionnels",
            "Les outils de la résolution de problème et amélioration continue"=>"Les outils de la résolution de problème et amélioration continue",
            "Gestion du personnel"=>"Gestion du personnel",
            "Gérer une situation conflictuelle"=>"Gérer une situation conflictuelle",
            "Mieux gérer son temps et ses priorités"=>"Mieux gérer son temps et ses priorités",
        ];

        $faculties = [];
        foreach ($facultyTypes as $acronym => $name) {
            $heFaculty = new HeFaculty();
            $heFaculty->setName($name)->setAcronym($acronym)->setDescription($name . '.' . $name);
            $faculties[] = $heFaculty;
            $manager->persist($heFaculty);
        }

        $manager->flush();
    }
}
