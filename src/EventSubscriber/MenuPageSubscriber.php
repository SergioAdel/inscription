<?php

namespace App\EventSubscriber;

use Twig\Environment;
use App\Repository\HePageRepository;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class MenuPageSubscriber implements EventSubscriberInterface
{
    private $twig;
    private $hePageRepository;
    public function __construct(Environment $twig, HePageRepository $hePageRepository)
    {
        $this->twig = $twig;
        $this->hePageRepository = $hePageRepository;
    }
    public function onControllerEvent(ControllerEvent  $event)
    {
        $this->twig->addGlobal('allPages', $this->hePageRepository->getPages());
    }

    public static function getSubscribedEvents()
    {
        return [
            ControllerEvent::class => 'onControllerEvent',
        ];
    }
}
