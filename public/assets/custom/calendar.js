$(function () {
  var Calendar = FullCalendar.Calendar;

  var calendarEl = document.getElementById("calendar");

  var calendar = new Calendar(calendarEl, {
    headerToolbar: {
      left: "prev,next today",
      center: "title",
      right: "dayGridMonth,timeGridWeek,timeGridDay",
    },
    footerToolbar: {
      left: "",
      center: "",
      right: "listDay, ,listMonth,listYear",
    },
    themeSystem: "bootstrap",
    events: [],
    locale: "fr",
    buttonText: {
      listMonth: "Planning du mois",
      listYear: "Planning de l'année",
      listDay: "Planning du jour",
    },
  });

  calendar.render();
});
