<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230405111321 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE he_degrees DROP FOREIGN KEY FK_E9368A89151F370A');
        $this->addSql('DROP INDEX IDX_E9368A89151F370A ON he_degrees');
        $this->addSql('ALTER TABLE he_degrees DROP facult_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE he_degrees ADD facult_id INT NOT NULL');
        $this->addSql('ALTER TABLE he_degrees ADD CONSTRAINT FK_E9368A89151F370A FOREIGN KEY (facult_id) REFERENCES he_faculties (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_E9368A89151F370A ON he_degrees (facult_id)');
    }
}
